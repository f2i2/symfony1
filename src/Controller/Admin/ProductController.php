<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductFormType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\ByteString;

#[Route('/my-admin')]
class ProductController extends AbstractController
{

    public function __construct(
        private ProductRepository $productRepository, 
        private RequestStack $requestStack,
        private EntityManagerInterface $entityManagerInterface
        ) {
    
    }

    #[Route('/products', name: 'app_admin_product')]
    public function index(): Response
    {

        return $this->render('admin/product/index.html.twig', [
            'products' => $this->productRepository->findAll(),
        ]);
    }


        #[Route('/products/delete{id}', name: 'app_admin_delete_product')]
    public function delete(int $id): Response
    {
        if ($id != null) {
            $current_product = $this->productRepository->find($id);
        }
        if (!$current_product) {
            $this->addFlash('danger', 'Le produit indexé est unexistant !');
            return $this->redirectToRoute('app_admin_product');
        }

        $this->entityManagerInterface->remove($current_product);
            $this->entityManagerInterface->flush();

                if($id) {
                    unlink("img/{$current_product->getPhotoName()}");
                }

        $this->addFlash('success', 'Le produit a été supprimé avec succès !');
        return $this->redirectToRoute('app_admin_product');

    }

    #[Route('/products/form', name: 'app_admin_product_form')]
    #[Route('/products/update/{id}', name: 'app_admin_edit_product_form')]
    public function form(int $id = null): Response
    {

        if ($id != null) {
            $current_product = $this->productRepository->find($id);
        }
        
        $entity = ($id!= null && $current_product) ? $current_product : new Product();

        if ($id!= null && $current_product == null) {
            $this->addFlash('danger', 'Le produit indexé n\'existe pas !');
            return $this->redirectToRoute('app_admin_product');
        }

        $entity->prevPhotoName = $entity->getPhotoName();


        $type = ProductFormType::class;
        $form = $this->createForm($type, $entity);

        $form->handleRequest($this->requestStack->getMainRequest());

        if($form->isSubmitted() && $form->isValid()){
            // dd($entity);
            $filename = ByteString::fromRandom(16)->lower();


            $file = $entity->getPhotoName();

            if($file instanceof UploadedFile){
                $fileExtension = $file->guessClientExtension();
                $file->move('img', "$filename.$fileExtension");

                $entity->setPhotoName("$filename.$fileExtension");

                if($id) {
                    unlink("img/{$entity->prevPhotoName}");
                }
            } else {
                 $entity->setPhotoName($entity->prevPhotoName);
            }



            // dd($filename, $entity);



            $this->entityManagerInterface->persist($entity);
            $this->entityManagerInterface->flush();

            $this->addFlash('success', ($id!= null && $current_product) ? 'Produit modifié avec succès !' :'Produit créé avec succès !');
            return $this->redirectToRoute('app_admin_product');
        }

        return $this->render('admin/product/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
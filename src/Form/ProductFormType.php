<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\HtmlSanitizer\Type\TextTypeHtmlSanitizerExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Currency;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Constraints\Valid;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // dd($options['data']->getId());
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Le nom est obligatoire',
                    ]),
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Le nom doit contenir au moins {{ limit }} caractères',
                        'max' => 255,
                        'maxMessage' => 'Le nom doit contenir au maximum {{ limit }} caractères',
                    ])
                ],
            ])
            ->add('description', TextareaType::class, [
                'constraints' => [
                                        new NotBlank([
                        'message' => 'La description est obligatoire',
                    ]),
                    
                ],
            ])
            ->add('photo_name', FileType::class, [
                'data_class' => null,
                'constraints' => $options['data']->getId() ? [] : [
                    new NotBlank([
                        'message' => 'La photo est obligatoire',
                    ]),

                    new Image([
                        'mimeTypesMessage'=> 'File format is not allowed',
                        'mimeTypes'=> [
                            'image/jpeg', 'image/gif', 'image/png'
                        ]
                    ])
                    
                ],
            ])
            ->add('price', MoneyType::class, [
                'constraints' => [
                                        new NotBlank([
                        'message' => 'Le prix est obligatoire',
                    ]),

                    new Positive([
                        'message' =>  'Price must be a number'
                    ]),
                    
                ],
                'invalid_message' => "Le prix n'est pas valide"
            ])
            ->add('quantity', IntegerType::class, [
                'constraints' => [
                                        new NotBlank([
                        'message' => 'La quantité est obligatoire',
                    ]),

                    new Positive([
                        'message' =>  'Quantity must be a number'
                    ]),
                    
                ],
            ]
            )
            ->add('category')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}